const mySql = require('mysql')

/**
 * Represents an function id that doesn't exist
 */
class NotFound extends Error {
    constructor(id, msg) {
        super(msg)
        this.id = id
    }
}
exports.NotFound = NotFound

/**
 * Get the results of a callback by id
 * @param {Environment} env The environment of the lambda
 * @param {number} id The identity number of the callback
 * @returns {object} 
 */
exports.resultFor = (env, id) => new Promise((resolve, reject) => {
    const conn = mySql.createConnection({
        user: env.dbUser,
        password: env.dbPassword,
        database: env.dbName,
        socketPath: env.dbSocketPath
    })
    conn.connect(err => {
        if (err) {
            conn.end()
            return reject(err)
        }
        conn.query(
            'SELECT last_run, result FROM funcs WHERE id=?;',
            [id],
            (err, results) => {
                if (err) {
                    conn.end()
                    return reject(err)
                }
                if (!results[0]) {
                    conn.end()
                    return reject(new NotFound(id, `Results for function id ${id} do not exist`))
                }
                conn.end()
                return resolve(results[0])
            }
        )
    })
})