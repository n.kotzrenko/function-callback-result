require('./types')

class EnvError extends Error {
    constructor(param, msg) {
        super(msg)
        this.param = param
    }
}
exports.EnvError = EnvError

/**
 * Extract parameter from environment
 * @param {string} paramName Parameter name 
 * @returns {[string, string]} Pair containing paramName and value
 */
const fromEnv = paramName => [paramName, process.env[paramName]]

/**
 * Assert that a parameter value isn't initial (empty or null)
 * The function throws an error in case the parameter is missing
 * @param {[string, string]} param Parameter name value pair
 * @returns {[string, string]}
 */
const assertNotInitial = param => {
    const [name, val] = param
    if (val === undefined || val === null) {
        throw new EnvError(param, `Parameter ${name} is empty`)
    } else {
        return param
    }
}

/**
 * Return the value of a given parameter name-value pair
 * @param {[string, string]} param Parameter name value pair
 * @returns {string} the value
 */
const paramValue = ([_, val]) => val

/**
 * Get parameter by name
 * @param {string} name Parameter name
 */
const getParam = name => paramValue(assertNotInitial(fromEnv(name)))

/**
 * @returns {Environment}
 */
exports.translateEnvironment = () => {
    return {
        dbHost: getParam('DB_HOST'),
        dbUser: getParam('DB_USER'),
        dbPassword: getParam('DB_PASSWORD'),
        dbName: getParam('DB_NAME'),
        dbSocketPath: getParam('DB_SOCKET_PATH')
    }
}