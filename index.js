const {translateEnvironment} = require('./env')
const {resultFor, NotFound} = require('./database')

exports.callbackResult = async (req, res) => {
    try {
        if (req.query.id === undefined || req.query.id === null) {
            return res.status(400).json({
                reason: 'No id query parameter supplied'
            })
        }
        console.log(`Searching for results of callback with id ${req.query.id}`)
        
        const env = translateEnvironment()
        const result = await resultFor(env, req.query.id)
        return res.status(200).json(result)
    } catch (err) {
        console.error(err)
        
        if (err instanceof NotFound) {
            return res.sendStatus(404).end()
        }
        return res.sendStatus(500).end()
    }
}