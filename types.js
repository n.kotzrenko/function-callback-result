 
/**
 * @typedef {{
 *  dbHost: string,
 *  dbUser: string,
 *  dbPassword: string,
 *  dbName: string,
 *  dbSocketPath: string
 * }} Environment
 */